import { Metrics } from '../../Themes'

export default {
  iconStyle: {
    marginHorizontal: Metrics.baseMargin
  }
}
