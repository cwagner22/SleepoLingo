export default {
  id: 0,
  title: 'Essentials',
  note: '\'kráp (male)/kâ (female)\' are placed at the end to increase the degree of politeness. Pom/Chan',
  cards: [
    {
      id: '6428a124-873f-4775-83c8-87b8ce8fe956',
      sentence: {
        original: 'Hello',
        translation: 'สวัสดี',
        transliteration: 'sà-wàt-dee'
      },
      fullSentence: {
        original: 'Hello, my name is Chris',
        translation: 'สวัสดี ผม ชื่อ คริส',
        transliteration: 'sà-wàt-dee pŏm chêu Chris'
      },
      image: require('../images/193795.jpg')
      // audio: require('./audio/6428a124-873f-4775-83c8-87b8ce8fe956.mp3'),
      // audiob: require('./audio/6428a124-873f-4775-83c8-87b8ce8fe956b.mp3'),
      // audioc: require('./audio/6428a124-873f-4775-83c8-87b8ce8fe956c.mp3')
    },
    {
      id: '61f31d9e-10eb-486e-9ab5-02861bee2e36',
      sentence: {
        original: 'Hi',
        translation: 'หวัดดี',
        transliteration: 'wàt dee'
      },
      fullSentence: {
        original: 'Hi, how are you?',
        translation: 'หวัดดี สบายดี ไหม',
        transliteration: 'wàt dee sà-baai dee măi'
      },
      image: require('../images/193795.jpg')
      // audio: require('./audio/6428a124-873f-4775-83c8-87b8ce8fe956.mp3'),
      // audiob: require('./audio/6428a124-873f-4775-83c8-87b8ce8fe956b.mp3'),
      // audioc: require('./audio/6428a124-873f-4775-83c8-87b8ce8fe956c.mp3')
    }
    // {
    //   id: 2,
    //   original: 'Hi!',
    //   translation: 'หวัดดี',
    //   transliteration: 'wàt dee',
    //   full: {
    //     original: 'Hi, how are you?',
    //     translation: 'หวัดดี สบายดี ไหม',
    //     transliteration: 'wàt dee sà-baai dee măi'
    //   },
    //   image: require('../images/0-2.jpg')
    // },
    // {
    //   id: 22,
    //   original: 'I\'m fine',
    //   translation: 'ฉัน สบายดี',
    //   transliteration: 'chăn sà-baai dee'
    // },
    // {
    //   id: 4,
    //   original: 'Goodbye',
    //   translation: 'ลาก่อน',
    //   transliteration: 'laa gòn',
    //   image: require('../images/0-4.jpg')
    // },
    // {
    //   id: 5,
    //   original: 'See you later',
    //   translation: 'เจอกัน',
    //   transliteration: 'jer gan',
    //   note: 'láew póp gan mài',
    //   image: require('../images/0-5.jpg')
    // },
    // {
    //   id: 6,
    //   original: 'See you tomorrow',
    //   translation: 'เจอกัน พรุ่งนี้',
    //   transliteration: 'jer gan prûng-née',
    //   image: require('../images/0-6.jpg')
    // },
    // {
    //   id: 7,
    //   original: 'Yes',
    //   translation: 'ใช่',
    //   transliteration: 'châi',
    //   image: require('../images/0-7.gif')
    // },
    // {
    //   id: 8,
    //   original: 'No',
    //   translation: 'ไม่ใช่',
    //   transliteration: 'mâi châi',
    //   image: require('../images/0-8.gif')
    // },
    // {
    //   id: 9,
    //   original: 'Sorry',
    //   translation: 'ขอโทษ',
    //   transliteration: 'kŏr-tôht',
    //   image: require('../images/0-9.png')
    // },
    // {
    //   id: 10,
    //   original: 'Please',
    //   translation: 'กรุณา',
    //   transliteration: 'gà-rú-naa',
    //   image: require('../images/0-10.jpg')
    // },
    // {
    //   id: 11,
    //   original: 'Thank you',
    //   translation: 'ขอบคุณ',
    //   transliteration: 'kòp kun',
    //   full: {
    //     original: 'Thank you very much',
    //     translation: 'ขอบคุณ มาก',
    //     transliteration: 'kòp kun mâak'
    //   },
    //   note: 'ขอบใจ (kòp jai) is used to thank someone with lower rank or status (less formal)',
    //   image: require('../images/0-11.jpg')
    // },
    // {
    //   id: 13,
    //   original: 'You\'re welcome',
    //   translation: 'ยินดี',
    //   transliteration: 'yin-dee',
    //   image: require('../images/0-13.jpg')
    // },
    // {
    //   id: 21,
    //   original: 'It doesn\'t matter/Nevermind/It’s okay',
    //   translation: 'ไม่ เป็นไร',
    //   transliteration: 'mâi bpen rai',
    //   explanation: [{
    //     original: 'ไม่',
    //     transliteration: 'mâi',
    //     translation: 'no'
    //   }, {
    //     original: 'เป็นไร',
    //     transliteration: 'bpen rai',
    //     translation: 'matter, problem'
    //   }],
    //   image: require('../images/0-21.jpg')
    // },
    // {
    //   id: 15,
    //   original: 'I understand',
    //   translation: 'ฉัน เข้าใจ',
    //   transliteration: 'chăn kâo jai',
    //   full: {
    //     original: 'I don\'t understand',
    //     translation: 'ฉัน ไม่ เข้าใจ',
    //     transliteration: 'chăn mâi kâo jai'
    //   },
    //   image: require('../images/0-14.jpg')
    // },
    // {
    //   id: 16,
    //   original: 'I don\'t know',
    //   translation: 'ผม ไม่ รู้',
    //   transliteration: 'pŏm mâi róo',
    //   image: require('../images/0-16.jpg')
    // },
    // {
    //   id: 17,
    //   original: 'What\'s up?',
    //   translation: 'ว่า ไง',
    //   transliteration: 'wâa ngai',
    //   image: require('../images/0-17.jpg')
    // },
    // {
    //   id: 18,
    //   original: 'I speak Thai',
    //   translation: 'ผม พูด ภาษาไทย',
    //   transliteration: 'pŏm pôot paa-săa tai'
    // },
    // {
    //   id: 19,
    //   original: 'Do you speak English?',
    //   translation: 'คุณ พูด ภาษาอังกฤษ ได้ ไหม',
    //   transliteration: 'kun pôot paa-săa ang-grìt dâai măi',
    //   image: require('../images/0-19.jpg')
    // },
    // {
    //   id: 20,
    //   original: 'Good night, sweet dreams',
    //   translation: 'ราตรีสวัสดิ์ ฝันหวาน',
    //   transliteration: 'raa-dtree sà-wàt făn wăan',
    //   image: require('../images/0-20.jpg')
    // }
  ]
}
