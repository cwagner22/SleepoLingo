export default [{
  id: 0,
  group: 'Basic',
  content: [
    require('./Basics/lesson1').default,
    require('./Basics/lesson2').default
    // require('./Basics/lesson3.json'),
    // require('./Basics/lesson4.json')
  ]
}, {
  id: 1,
  group: 'Basic++',
  content: []
}, {
  id: 2,
  group: 'Vocabulary/Categories',
  content: []
}, {
  id: 3,
  group: 'Advanced',
  content: []
}]
